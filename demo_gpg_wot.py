#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Copyright (c) 2013, jben <jben@jben.info>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from graph import graph

cr=(.8,.0,.0)
cg=(.0,.8,.0)
cy=(.8,.8,.0)
ck=(.0,.0,.0)

g=graph(15)
g.set_export_prefix('demos/demo_gpg_wot/')

g.edge_color=ck

g.colors[0]=cr

g.avancer_de(5)


# initial network
g.ajouter_lien(1,13)
g.ajouter_lien(1,7)
g.ajouter_lien(2,7)
g.ajouter_lien(2,8)
g.ajouter_lien(3,9)
g.ajouter_lien(3,10)
g.ajouter_lien(4,10)
g.ajouter_lien(4,11)
g.ajouter_lien(5,10)
g.ajouter_lien(5,11)
g.ajouter_lien(5,12)
g.ajouter_lien(6,12)
g.ajouter_lien(6,13)
g.ajouter_lien(10,11)
g.ajouter_lien(12,13)
g.ajouter_lien(12,14)
g.ajouter_lien(5,14)
g.ajouter_lien(11,14)


g.avancer_de(10)

g.perturber(1)
g.avancer_de(10)

# je signe 1
g.ajouter_lien(0,1)
g.colors[1]=cy
g.avancer_de(15)

# je signe 2
g.ajouter_lien(0,2)
g.colors[2]=cy
g.avancer_de(10)

# je fais confiance a 2
g.colors[2]=cg
g.avancer_de(15)

# je signe 3,4,5,6
g.ajouter_lien(0,3)
g.ajouter_lien(0,4)
g.ajouter_lien(0,5)
g.ajouter_lien(0,6)
g.colors[3]=cy
g.colors[4]=cy
g.colors[5]=cy
g.colors[6]=cy
g.avancer_de(15)

# je fais confiance a 3,4,5,6
g.colors[3]=cg
g.colors[4]=cg
g.colors[5]=cg
g.colors[6]=cg
g.colors[10]=cy
g.avancer_de(20)

# je fais confiance a 10
g.colors[10]=cg
g.colors[11]=cy
g.avancer_de(15)

# nouveau lien
g.ajouter_lien(10,14)
g.avancer_de(15)

# je fais confiance a 11
g.colors[11]=cg
g.colors[14]=cy
g.avancer_de(30)
