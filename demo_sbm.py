#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Copyright (c) 2013, jben <jben@jben.info>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from graph import graph

import numpy as np

n=100

alpha = [.25,.25,.5]
PI = np.matrix([[.12,.08,.00],[.08,.00,.04],[.00,.04,.00]])

colors=((.8,.0,.0),(.0,.8,.0),(.0,.0,.8))

# au début il y a les noeuds

g=graph(n)
g.set_export_prefix('demos/demo_sbm/')

g.text=u'Au commencement il y a les nœuds…'
g.colors=[(0.,0.,0.)]*n
g.edge_color=(.0,.0,.0)
g.edge_width=.4
g.avancer_de(8)

g.text=''
g.avancer_de(1)

# puis il y a les couleurs

g.text=u'puis il y a les couleurs affectées aléatoirement…'
N0=np.random.binomial(n,alpha[0])
N1=np.random.binomial(n-N0,alpha[1]/(alpha[1]+alpha[2]))
N2=n-N1-N0

def grp(k):
    if(k<N0):
        return(0)
    if(k<N0+N1):
        return(1)
    return(2)

for i in np.argsort(np.random.uniform(size=n)):
    g.colors[i]=colors[grp(i)]
    g.avancer_de(.1)


g.avancer_de(3)
g.text=''

# puis il y a les liens

Ndeb=(0,N0,N0+N1)
Nfin=(N0,N0+N1,n)

txts=[[
       u'entre nœud rouge et nœud rouge, il y a 12% de proba d\'avoir un lien…',
       u'entre nœud rouge et nœud vert, il y a 8% de proba d\'avoir un lien…',
       u''],[
       u'',
       u'',
       u'entre nœud vert et nœud bleu, il y a 4% de proba d\'avoir un lien…'],[
       u'',
       u'',
       u'']]



for (q,l) in  [(0,0),(0,1),(1,2)]:
    #for i in range(Ndeb[q],Nfin[q]):
    #    for j in range(Ndeb[l],Nfin[l]):
    #        if i<j:
    #            g.non_real_links.append((i,j))

    g.avancer_de(3)
    g.text=txts[q][l]
    g.avancer_de(3)

    for i in range(Ndeb[q],Nfin[q]):
        for j in range(Ndeb[l],Nfin[l]):
            if i<j:
                if(np.random.uniform()<PI[q,l]):
                    g.ajouter_lien(i,j)
                    g.avancer_de(.1)

    g.non_real_links=[]
    g.avancer_de(2)
    g.text=''

g.avancer_de(2)
g.text=u'tous les autres associations de couleurs sont impossibles…'
g.avancer_de(5)

g.text=u'ceci est le graph obtenu…'
g.avancer_de(5)

g.text=u'mais les couleurs ne sont pas observées…'
g.colors=[(0.,0.,0.)]*n

g.avancer_de(5)
g.text=u'le but de l\'inférence est de retrouver la structure.'
g.avancer_de(10)
