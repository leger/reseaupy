#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Copyright (c) 2013, jben <jben@jben.info>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from graph import graph

# 0 _                _ 9
# 1 _\              /_ 8
# 2 _\\            //_ 7
# 3 _\\\          ///_ 6
#    \\\\        //// 
#      4__________5

g=graph(10)

g.set_export_prefix('demos/demo4/')

for i in range(0,4):
    g.colors[i]=(.0,.8,.0)
g.colors[4]=(.0,.8,.4)

for i in range(6,10):
    g.colors[i]=(.0,.0,.8)
g.colors[5]=(.0,.4,.8)

g.avancer_de(5)

for i in range(0,4):
    g.ajouter_lien(i,4)
    g.avancer_de(.1)

for i in range(6,10):
    g.ajouter_lien(i,5)
    g.avancer_de(.1)

g.ajouter_lien(4,5)

g.avancer_de(20)
