#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Copyright (c) 2013, jben <jben@jben.info>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from graph import graph

lAdd=[];

for i in range(99):
    lAdd.append((i,i+1))


lAdd.append((100-1,97-1))
lAdd.append((100-1,95-1))
lAdd.append((100-1,93-1))
lAdd.append((99-1,92-1))
lAdd.append((99-1,90-1))
lAdd.append((98-1,89-1))
lAdd.append((98-1,87-1))
lAdd.append((97-1,86-1))
lAdd.append((96-1,85-1))
lAdd.append((96-1,83-1))
lAdd.append((95-1,82-1))
lAdd.append((94-1,81-1))
lAdd.append((94-1,79-1))
lAdd.append((93-1,78-1))
lAdd.append((92-1,77-1))
lAdd.append((91-1,76-1))
lAdd.append((91-1,74-1))
lAdd.append((90-1,73-1))
lAdd.append((89-1,72-1))
lAdd.append((88-1,71-1))
lAdd.append((88-1,69-1))
lAdd.append((87-1,68-1))
lAdd.append((86-1,67-1))
lAdd.append((85-1,66-1))
lAdd.append((84-1,65-1))
lAdd.append((84-1,63-1))
lAdd.append((83-1,62-1))
lAdd.append((82-1,61-1))
lAdd.append((81-1,60-1))
lAdd.append((80-1,59-1))
lAdd.append((80-1,57-1))
lAdd.append((79-1,56-1))
lAdd.append((78-1,55-1))
lAdd.append((77-1,54-1))
lAdd.append((76-1,53-1))
lAdd.append((75-1,52-1))
lAdd.append((75-1,50-1))
lAdd.append((74-1,49-1))
lAdd.append((73-1,48-1))
lAdd.append((72-1,47-1))
lAdd.append((71-1,46-1))
lAdd.append((70-1,45-1))
lAdd.append((70-1,43-1))
lAdd.append((69-1,42-1))
lAdd.append((68-1,41-1))
lAdd.append((67-1,40-1))
lAdd.append((66-1,39-1))
lAdd.append((65-1,38-1))
lAdd.append((64-1,37-1))
lAdd.append((64-1,35-1))
lAdd.append((63-1,34-1))
lAdd.append((62-1,33-1))
lAdd.append((61-1,32-1))
lAdd.append((60-1,31-1))
lAdd.append((59-1,30-1))
lAdd.append((58-1,29-1))
lAdd.append((58-1,27-1))
lAdd.append((57-1,26-1))
lAdd.append((56-1,25-1))
lAdd.append((55-1,24-1))
lAdd.append((54-1,23-1))
lAdd.append((53-1,22-1))
lAdd.append((52-1,21-1))
lAdd.append((51-1,20-1))
lAdd.append((51-1,18-1))
lAdd.append((50-1,17-1))
lAdd.append((49-1,16-1))
lAdd.append((48-1,15-1))
lAdd.append((47-1,14-1))
lAdd.append((46-1,13-1))
lAdd.append((45-1,12-1))
lAdd.append((44-1,11-1))
lAdd.append((44-1,9-1))
lAdd.append((43-1,8-1))
lAdd.append((42-1,7-1))
lAdd.append((41-1,6-1))
lAdd.append((40-1,5-1))
lAdd.append((39-1,4-1))
lAdd.append((38-1,3-1))
lAdd.append((37-1,2-1))
lAdd.append((36-1,1-1))

g=graph(100)

g.set_export_prefix('demos/demo1/')

g.f_frott_V = .01 # N.(m.s^-1)^-1
g.f_frott_V2 = .001 # N.(m.s^-1)^-2

g.avancer_de(5)

for couple in lAdd:
    g.ajouter_lien(couple[0],couple[1])
    g.avancer_de(.1)

g.avancer_de(70)
