#!/usr/bin/python
# -*- encoding: utf-8 -*-

# Copyright (c) 2013, jben <jben@jben.info>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import matplotlib.pyplot as plt
from etat import etat

class DtIsTooBig(Exception):
    "Exception raised when dt is too big"

    pass


class graph:


    def __init__(self,n):
        self.rayon = np.sqrt(n)*.02 # m
        self.C_rep = 1.             # N.m^2
        self.l0 = .07*np.sqrt(n)    # m
        self.k_ref = .5             # N.m^-1
        self.F_centrale = 1.        # N
        self.f_frott_V = .025       # N.(m.s^-1)^-2
        self.f_frott_V2 = .025      # N.(m.s^-1)^-1
        self.masse = .1             # kg

        self.fixed_nodes = []

        self.dtpref=.04 # s (25 fps)
        self.export='/tmp/img/%09d.png'
        self.t=0
        self.tcont=0
        self.n=0
        self.colors=[(0.,0.,1.)]*n
        self.edge_color=(1.,0.,0.)
        self.edge_width=.5

        self.tps_echelle=5
        self.taille_max=np.sqrt(n)
        self.fig=plt.figure(figsize=(6,6))
        self.sp=self.fig.add_subplot(1,1,1)
        self.text=''
        self.textfig=self.fig.text(.5,.05,'',ha='center')
        self.dpi=100

        self.non_real_links=[]
        self.non_real_links_color=(.5,.5,.5)

        self.adjM = np.matrix(np.zeros((n,n)))
        self.e = etat(n)
        return None

    def set_export_prefix(self,prefix):
        self.export = prefix + '%09d.png'

    def exporter(self):
        taille=max(np.max(np.abs(self.e.x)),np.max(np.abs(self.e.y)))
        rho = np.exp(-self.dtpref/self.tps_echelle)
        self.taille_max = rho*self.taille_max + (1-rho)*taille

        K=1.2*self.taille_max
        self.sp.axis((-K,K,-K,K))
        self.sp.axis('off')

        links = self.adjM.nonzero()

        for k in range(links[0].size):
            i=links[0][0,k]
            j=links[1][0,k]
            if(i<j):
                line=plt.Line2D((self.e.x[i],self.e.x[j]),(self.e.y[i],self.e.y[j]),color=self.edge_color,pickradius=self.rayon,linewidth=self.edge_width*self.adjM[i,j])
                self.sp.add_line(line)


        
        for i in range(self.adjM.shape[0]):
            circle=plt.Circle((self.e.x[i],self.e.y[i]),radius=self.rayon,color=self.colors[i],aa=True,alpha=1.)
            self.sp.add_patch(circle)
        
        self.textfig.set_text(self.text)
        
        self.fig.savefig(self.export % self.n,dpi=self.dpi)
        self.sp.clear()
        self.n += 1


    def ajouter_lien(self,i,j):
        self.adjM[i,j]=self.adjM[j,i]=1

    def avancer_de(self,delta_t):
        self.avancer_jusqua(self.tcont+delta_t)

    def avancer_jusqua(self,to_t):
        if self.n==0:
            self.exporter()
        while(self.t<to_t):
            self.avancer_de_dt(self.dtpref)
            self.exporter()
        self.tcont=to_t

    def avancer_de_dt(self,dt):
        #print ("avancer de",dt)
        try:
            k1=self.derivee(self.e,dt)
            k2=self.derivee(self.e+k1*(dt/2),dt)
            k3=self.derivee(self.e+k2*(dt/2),dt)
            k4=self.derivee(self.e+k3*dt,dt)
        except DtIsTooBig:
            #print ("  --> refus de",dt);
            self.avancer_de_dt(dt/2)
            self.avancer_de_dt(dt/2)
        else:
            #print ("  --> ok de",dt)
            self.e += (k1+k2*2+k3*2+k4)*(dt/6)
            self.t+=dt
            print("t = %.10f \t dt = %.10f"%(self.t,dt))
        return None

    def perturber(self,amplitude=1.):

        perturbation = amplitude * self.energie_mechanique()
        vitesse = np.sqrt(perturbation/self.adjM.shape[0])

        r = np.random.uniform(0,2*np.pi,self.adjM.shape[0])

        self.e.Vx += vitesse * np.cos(r).reshape((self.adjM.shape[0],1))
        self.e.Vy += vitesse * np.sin(r).reshape((self.adjM.shape[0],1))

    def derivee(self,e,dt):
        (force_x,force_y) = self.force(e,dt)
        deriv = etat(
                     x = e.Vx,
                     y = e.Vy,
                     Vx= force_x/self.masse,
                     Vy= force_y/self.masse
                    )
        deriv.x[self.fixed_nodes] = 0.
        deriv.y[self.fixed_nodes] = 0.
        deriv.Vx[self.fixed_nodes] = 0.
        deriv.Vy[self.fixed_nodes] = 0.

        return deriv

    def energie_mechanique(self):
        (dir_x,dir_y,distMat,distMatRay)=self.precalcul_dir_dist(self.e)

        energie_rep = np.sum(self.C_rep*1./distMatRay)

        energie_ressort = .5*self.k_ref*np.sum(np.multiply(self.adjM,np.power(distMat-self.l0,2)))

        energie_centrale = self.F_centrale*np.sum(np.sqrt(np.power(self.e.x,2)+np.power(self.e.y,2)))

        return (energie_rep+energie_ressort+energie_centrale)



    def precalcul_dir_dist(self,e):
        dir_x = e.x.T-e.x
        dir_y = e.y.T-e.y

        distMat=np.sqrt(np.power(dir_x,2)+np.power(dir_y,2))
        distMat+=np.max(distMat)*np.eye(distMat.shape[0])

        distMatRay = distMat-2*self.rayon

        return (dir_x,dir_y,distMat,distMatRay)
    
    def force(self,e,dt):
        
        (dir_x,dir_y,distMat,distMatRay)=self.precalcul_dir_dist(e)

        dir_x_n = dir_x/distMat
        dir_y_n = dir_y/distMat


        norme_vit = np.sqrt(np.power(e.Vx,2)+np.power(e.Vy,2))
        vmax = np.max(norme_vit)

        dmin = np.min(distMatRay)

        #print("    --> do",dmin,vmax,dt)
        if dmin < vmax*dt*3 :
            raise DtIsTooBig    

        # calcul des forces repulsives

        force_rep=self.C_rep*np.power(distMatRay,-2);
        force_rep_x=-np.sum(np.multiply(force_rep,dir_x_n),1)
        force_rep_y=-np.sum(np.multiply(force_rep,dir_y_n),1)

        # calcul des forces de ressorts

        force_ressort = self.k_ref*np.multiply(self.adjM,distMat-self.l0)
        force_ressort_x = np.sum(np.multiply(force_ressort,dir_x_n),1)
        force_ressort_y = np.sum(np.multiply(force_ressort,dir_y_n),1)

        # calcul des forces centrales

        norme_pos = np.sqrt(np.power(e.x,2)+np.power(e.y,2))
        force_centrale_x = - self.F_centrale*e.x/norme_pos
        force_centrale_y = - self.F_centrale*e.y/norme_pos

        # calcul des forces de frottement

        force_frott_x = - self.f_frott_V2*np.multiply(norme_vit,e.Vx) - self.f_frott_V*e.Vx
        force_frott_y = - self.f_frott_V2*np.multiply(norme_vit,e.Vy) - self.f_frott_V*e.Vy

        # total

        force_totale_x = force_rep_x + force_ressort_x + force_centrale_x + force_frott_x
        force_totale_y = force_rep_y + force_ressort_y + force_centrale_y + force_frott_y

        # fixed nodes

        force_totale_x[self.fixed_nodes]=0.
        force_totale_y[self.fixed_nodes]=0.

        # verif pas
        
        vmax2 = np.sqrt(np.max(
                        np.power(e.Vx + (dt/self.masse)*force_totale_x,2)
                        +
                        np.power(e.Vy + (dt/self.masse)*force_totale_y,2)
                       ))

        if dmin < vmax2*dt*3 :
            raise DtIsTooBig    

        return (force_totale_x, force_totale_y)









